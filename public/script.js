
$(document).ready(function() {

  let searchParams = new URLSearchParams(window.location.search);
  if(!searchParams.has('internal')) {
    $('.internal').hide();
  }
  
  $.getJSON("preisliste.json", function(preise) {

    /*
  let papierkostenSW = {
  	"standard80":0.12,
  	"galaxy115":0.14,
  	"standard160":0.25,
  	"galaxy200":0.25,
  	"galaxy300":0.34,
  	"senator":0.32,
  	"sticker":0.50,
  	"kalandriert100":0.19,
    "kalandriert160":0.22
  }
  
  let papierkostenF = {
  	"standard80":0.14,
  	"galaxy115":0.16,
  	"standard160":0.30,
  	"galaxy200":0.28,
  	"galaxy300":0.36,
  	"senator":0.34,
  	"sticker":0.60,
  	"kalandriert100":0.20,
    "kalandriert160":0.25
  }
  
  let bindungsartkosten = {
  	"buchbindung":0.20,
    "bindefolie":1.35,
    "klebestreifen":1.00
  }
  
  let visitenkartenkosten = {
  	"min" : 6,
    "alle30" : 0.60
  }
  
  let grossformatkosten = {
  	"A2klebeband" : 2.50,
    "A1klebeband" : 5,
    "A0klebeband" : 7.50,
    "A2plotter" : 5.50,
    "A1plotter" : 11,
    "A0plotter" : 15
  }
    
  */

    var einzelmaterialienGewaehlt = 0;

    $('input[id="einzelmaterialien"]').click( function() {

      $("#einzelmaterialienwerte").empty();

      var einst = "<br>";

      einzelmaterialienGewaehlt = $('input[id="einzelmaterialien"]:checked').val();

      if(einzelmaterialienGewaehlt) {
        einst += `
        <label for="anzahlCD">CD Hüllen / sleeve: </label>
        <input type="number" class="auftragswertMaterialien form-control" id="anzahlCD" name="anzahlCD">
        <label for="anzahlA4Nb">A4 Notizblock / notepad: </label>
        <input type="number" class="auftragswertMaterialien form-control" id="anzahlA4Nb" name="anzahlA4Nb">
        <label for="anzahlKlNb">Kleiner Notizblock / Small notepad: </label>
        <input type="number" class="auftragswertMaterialien form-control" id="anzahlKlNb" name="anzahlKlNb">

        <label for="anzahlA4Bs">A4 Briefumschlag / envelope: </label>
        <input type="number" class="auftragswertMaterialien form-control" id="anzahlA4Bs" name="anzahlA4Bs">
        <label for="anzahlA5Bs">A5 Briefumschlag / envelope: </label>
        <input type="number" class="auftragswertMaterialien form-control" id="anzahlA5Bs" name="anzahlA5Bs">
        `;
      }

      $("#einzelmaterialienwerte").append(einst);
    });

    var art = "Sonstiges";
  
    $('input[name="auftragsart"]').click( function() {
      art = $('input[name="auftragsart"]:checked').val();
      $("#auftragswerte").empty();
      
      var einst = "<br>";
      
      switch(art) {
        case "abschlussarbeit":

          einst += '<label for="exemplare">Anzahl Exemplare: </label>';
          einst += '<input type="number" class="auftragswertAbschlussarbeit form-control" id="exemplare" name="exemplare"> ';

        
          einst += '<label for="seitenzahlF">Anzahl Seiten: </label>';
          einst += '<input type="number" class="auftragswertAbschlussarbeit form-control" id="seitenzahlF" name="seitenzahlF"> ';

          einst += '<select class="auftragswertAbschlussarbeit form-control" id="plex" name="plex">';
          einst += '<option value="simplex">Einseitig</option>';
          einst += '<option value="duplex">Beidseitig</option>';
          einst += '</select><br>'
          
          //Laut Simone: Fuer Abschlussarbeiten immer Farbigkosten uebernehmen
          //einst += '<label for="seitenzahlF">Anzahl farbige Seiten: </label>';
          //einst += '<input type="number" class="auftragswertAbschlussarbeit form-control" id="seitenzahlF" name="seitenzahlF"><br>';
          
          einst += '<label for="papierartCover">Papierart Deckblatt: </label>';
          einst += '<select class="auftragswertAbschlussarbeit form-control" id="papierartCover" name="papierartCover">';
          einst += '<option value="standard160">Standard - 160gr/m<sup>2</sup></option>';
          einst += '<option value="galaxy200">Galaxy - 200gr/m<sup>2</sup></option>';
          einst += '<option value="senator">Senator - 250gr/m<sup>2</sup></option>';
          einst += '<option value="galaxy300">Galaxy - 300gr/m<sup>2</sup></option>';
          einst += '</select>'
          
          einst += '<select class="auftragswertAbschlussarbeit form-control" id="dbFarbe" name="dbFarbe">';
          einst += '<option value="f">Farbe</option>';
          einst += '<option value="sw">S/W</option>';
          einst += '</select><br>'
          
          einst += '<label for="papierartInnen">Papierart Innenseiten: </label>';
          einst += '<select class="auftragswertAbschlussarbeit form-control" id="papierartInnen" name="papierartInnen">';
          einst += '<option value="standard80">Standard - 80gr/m<sup>2</sup></option>';
          einst += '<option value="kalandriert100">Kalandriert - 100gr/m<sup>2</sup></option>';
          einst += '<option value="galaxy115">Galaxy - 115gr/m<sup>2</sup></option>';
          einst += '</select><br>'
          
          einst += '<label for="bindung">Bindungsart: </label>';
          einst += '<select class="auftragswertAbschlussarbeit form-control" id="bindung" name="bindung">';
          einst += '<option value="buchbindung">Buchbindung mit Rückenbeschriftung</option>';
          einst += '<option value="bindefolie">Klebestreife mit Bindefolie</option>';
          einst += '<option value="klebestreifen">Klebestreife</option>';
          einst += '<option value="kein">Keine Bindung</option>';
          einst += '</select><br>'
      
          $('input[name="arbeitsstunden"]').val('0.5');
          break;
          
        case "visitenkarten":
          einst += '<label for="anzahl">Menge: </label>';
          einst += '<input type="number" class="auftragswertAbschlussarbeit form-control" id="anzahl" name="anzahl"><br>';
          
          $('input[name="arbeitsstunden"]').val('0');
          break;
        
        case "sticker":
          einst += '<label for="anzahl">Anzahl A3-Blätter (mehrere Stickers pro Blatt): </label>';
          einst += '<input type="number" class="auftragswertAbschlussarbeit form-control" id="anzahl" name="anzahl">';
          
          einst += '<select class="auftragswertAbschlussarbeit form-control" id="farbe" name="farbe">';
          einst += '<option value="f">Farbe</option>';
          einst += '<option value="sw">S/W</option>';
          einst += '</select><br>'

          
          $('input[name="arbeitsstunden"]').val('0.25');
          break;
        
        case "poster":
        
          einst += '<label for="anzahl">Menge: </label>';
          einst += '<input type="number" class="auftragswertAbschlussarbeit form-control" id="anzahl" name="anzahl"><br>';
        
          einst += '<label for="groesse">Seitenformat: </label>';
          einst += '<select class="auftragswertAbschlussarbeit form-control" id="groesse" name="groesse">';
          einst += '<option value="A2">A2</option>';
          einst += '<option value="A1">A1</option>';
          einst += '<option value="A0">A0</option>';
          einst += '</select><br>'
        /*
          einst += '<label for="papierart">Papierart: </label>';
          einst += '<select class="auftragswertAbschlussarbeit form-control" id="papierart" name="papierart">';
          einst += '<option value="standard80">Standard - 80gr/m<sup>2</sup></option>';
          einst += '<option value="kalandriert100">Kalandriert - 100gr/m<sup>2</sup></option>';
          einst += '<option value="galaxy115">Galaxy - 115gr/m<sup>2</sup></option>';
          einst += '</select><br>'
          */
          einst += '<label for="herstellung">Herstellung: </label>';
          einst += '<select class="auftragswertAbschlussarbeit form-control" id="herstellung" name="herstellung">';
          einst += '<option value="klebeband">Zusammengeführte A3-Blätter</option>';
          einst += '<option value="plotter">Plotter (eventuelle Farbenabweichung und bedingte Verfügbarkeit)</option>';
          einst += '</select><br>'
          
          $('input[name="arbeitsstunden"]').val('0.5');
          break;
        
        case "sonstiges":
          einst += '<label for="anzahl">Menge: </label>';
          einst += '<input type="number" class="auftragswertSonstiges form-control" id="anzahl" name="anzahl">';
          
          einst += '<select class="auftragswertSonstiges form-control" id="farbe" name="farbe">';
          einst += '<option value="f">Farbe</option>';
          einst += '<option value="sw">S/W</option>';
          einst += '</select><br>'
        
          einst += '<label for="groesse">Seitenformat: </label>';
          einst += '<select class="auftragswertSonstiges form-control" id="groesse" name="groesse">';
          einst += '<option value="A3">A3</option>';
          einst += '<option value="A4">A4</option>';
          einst += '<option value="A5">A5</option>';
          einst += '</select><br>'
        
          einst += '<label for="papierart">Papierart: </label>';
          einst += '<select class="auftragswertSonstiges form-control" id="papierart" name="papierart">';
          einst += '<option value="standard80">Standard - 80gr/m<sup>2</sup></option>';
          einst += '<option value="kalandriert100">Kalandriert - 100gr/m<sup>2</sup></option>';
          einst += '<option value="galaxy115">Galaxy - 115gr/m<sup>2</sup></option>';
          einst += '<option value="standard160">Standard - 160gr/m<sup>2</sup></option>';
          einst += '<option value="kalandriert160">Kalandriert - 160gr/m<sup>2</sup></option>';
          einst += '<option value="galaxy200">Galaxy - 200gr/m<sup>2</sup></option>';
          einst += '<option value="senator">Senator - 250gr/m<sup>2</sup></option>';
          einst += '<option value="galaxy300">Galaxy - 300gr/m<sup>2</sup></option>';
          einst += '<option value="zeta300">Zeta Leinen A4 - 300gr/m<sup>2</sup></option>';
          einst += '</select><br>'
          
          $('input[name="arbeitsstunden"]').val('0.5');
          break;
            
      }
      
      $("#auftragswerte").append(einst);
    });
    
    $('#berechnen').click( function() {
      var mitarbeiterkosten = $('input[name="arbeitsstunden"]').val() * 15;	//	anpassen
      
      var nettopreis = mitarbeiterkosten;

      //Kosten von Einzelmaterialien
      if(einzelmaterialienGewaehlt) {
        nettopreis += $('#anzahlCD').val() * preise.einzelmaterialien['CDHuelle'];

        nettopreis += $('#anzahlA4Nb').val() * preise.einzelmaterialien['A4Notizblock'];

        nettopreis += $('#anzahlKlNb').val() * preise.einzelmaterialien['klNotizblock'];

        nettopreis += $('#anzahlA4Bs').val() * preise.einzelmaterialien['A4Briefumschlag'];

        nettopreis += $('#anzahlA5Bs').val() * preise.einzelmaterialien['A5Briefumschlag'];
      }
      
      switch(art) {
        case "abschlussarbeit":
        /*
          var seitenfaktor = 1;
          if($('#plex').val() === "duplex") {
            seitenfaktor = 2;
          }*/  
          
          var einzelexemplarPreis = 0;
          
          //var swKosten = Math.ceil( ( $('#seitenzahl').val() - $('#seitenzahlF').val() ) / 2 ) * preise.papierkostenSW[$('#papierartInnen').val()];
          var fKosten = Math.ceil( $('#seitenzahlF').val() / 2 ) * preise.papierkostenF[$('#papierartInnen').val()];
            
          einzelexemplarPreis += /*swKosten +*/ fKosten;

          if($('#dbFarbe').val() === 'sw') {
            einzelexemplarPreis += preise.papierkostenSW[$('#papierartCover').val()];
          } else {
            einzelexemplarPreis += preise.papierkostenF[$('#papierartCover').val()];
          }
          
          einzelexemplarPreis += preise.bindungsartkosten[$('#bindung').val()];

          nettopreis += einzelexemplarPreis * $('#exemplare').val();
          
          
          break;
          
        case "visitenkarten":
        
          nettopreis += preise.visitenkartenkosten.min;
        
          var zusatzfaktor = $('#anzahl').val() - 30;
          
          
          if(zusatzfaktor > 0) {
            zusatzfaktor = Math.ceil(zusatzfaktor/30);
            nettopreis += preise.visitenkartenkosten.alle30 * zusatzfaktor;
          }        
                    
          break;
          
        case "sticker":
          
          if($('#farbe').val() === 'sw') {
            nettopreis += $('#anzahl').val() * preise.papierkostenSW.sticker;
          } else {
            nettopreis += $('#anzahl').val() * preise.papierkostenF.sticker;
          }
          
          break;
          
        case "poster":
          var mengenfaktor = 2;
          switch($('#groesse').val()) {
            case 'A0': 
              mengenfaktor = 8; 
              break;
              
            case 'A1': 
              mengenfaktor = 4; 
              break;
              
            case 'A2': 
              mengenfaktor = 2; 
              break;
          }

          //nettopreis += mengenfaktor * $('#anzahl').val() * preise.papierkostenF[$('#papierart').val()];

          nettopreis += $('#anzahl').val() * ( preise.grossformatkosten[$('#groesse').val() + $('#herstellung').val()] );
          break;
          
        case "sonstiges":
          
          var mengenfaktor = 1;
          switch($('#groesse').val()) {
            case 'A3': 
              mengenfaktor = 1; 
              break;
              
            case 'A4': 
              mengenfaktor = 0.5; 
              break;
              
            case 'A5': 
              mengenfaktor = 0.25; 
              break;
          }

          var papierpreis = 0;
          if($('#farbe').val() === 'sw') {
            papierpreis = preise.papierkostenSW[$('#papierart').val()];
          } else {
            papierpreis += preise.papierkostenF[$('#papierart').val()];
          }
          nettopreis += Math.ceil( mengenfaktor * $('#anzahl').val() ) * papierpreis;

      }
      
      nettopreis = Math.ceil(100 * nettopreis) / 100;
      var bruttopreis = Math.ceil(107 * nettopreis) / 100;
      
      $("#nettopreis").empty();
      $("#nettopreis").append(nettopreis);
      $("#nettopreis").append(" €");
      
      $("#bruttopreis").empty();
      $("#bruttopreis").append(bruttopreis);
      $("#bruttopreis").append(" €");
      
    });
    /*
    $(document).on( "click", "#papierartInnen", function() {
      if ($(this).val() === 'standard80') {
          document.getElementById('seitenzahlF').removeAttribute("disabled");
      } else {
          document.getElementById('seitenzahlF').setAttribute("disabled", true);
      }
    });
    */
    $(document).on( "click", "#herstellung", function() {
      if ($(this).val() === 'plotter') {
          document.getElementById('papierart').setAttribute("disabled", true);
      } else {
          document.getElementById('papierart').removeAttribute("disabled");
      }
    });
  });

  
});